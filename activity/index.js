




/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	function getUserDetails(){
	let fullName = prompt("What is your name? ");
	let age = prompt("What is your age? ");
	let location = prompt("Where do you live? ");

	alert("Thank you for entering your information!");
	console.log("Hello, " + fullName);
	console.log("You are " + age + " years old");
	console.log("You live in " + location);
}

getUserDetails();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	function likedBands(){
	let band1 = "1. FM Static";
	let band2 = "2. Boys Like Girs";
	let band3 = "3. Red Jumpsuit Apparatus";
	let band4 = "4. My Chemical Romance";
	let band5 = "5. Paramore";

	console.log(band1);
	console.log(band2);
	console.log(band3);
	console.log(band4);
	console.log(band5);
}

likedBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	
function likedMovies(){
	let movies = ["1. Harry Potter and the Deathly Hollows Part 2", "2. Harry Potter and the Prisoner of Azkaban", "3. Harry Potter and the Goblet of Fire", "4. Harry Potter and the Half-Blood Prince", "5. Harry Potter and the Chamber of Secrets"];
	let ratings = [96, 90, 88, 84, 82];

	console.log(movies[0]);
	console.log("Rotten Tomato Ratings: " + ratings[0] + "%");
	console.log(movies[1]);
	console.log("Rotten Tomato Ratings: " + ratings[1] + "%");
	console.log(movies[2]);
	console.log("Rotten Tomato Ratings: " + ratings[2] + "%");
	console.log(movies[3]);
	console.log("Rotten Tomato Ratings: " + ratings[3] + "%");
	console.log(movies[4]);
	console.log("Rotten Tomato Ratings: " + ratings[4] + "%");


}

likedMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function (){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name: "); 
	let friend2 = prompt("Enter your second friend's name: "); 
	let friend3 = prompt("Enter your third friend's name: ");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


printFriends();