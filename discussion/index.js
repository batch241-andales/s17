//alert("Hello");

/*
FUNCTIONS
	Functions in javascript are lines/block of codes that tell our device/application to perform a certain task when called/invoked

	Function are mostly created to create complicate task to run several lines of code in succession.

	Functions are also used to prevent repeating lines/blocks of codes that perform the same task/function
*/

//Function Declaration
	//(function statement) defines a function with the specified parameters

/*
	Syntax:
	function functionName() {
		codeblock(statement)
	}

	function keyword - used to defined a javascript functions
	functionName - the function name. Function are named to be able to use later in the code
	function block ({}) - the statements which comprise the body of the function. This is where the code to be executed
	
*/


function printName() {
	console.log("My name is John!");
}

//Function Invocation
/*
		The code block and statements inside a function is not immediately executed when the function is defined. Thec ode block and statements inside a function is executed when the function is invoked or called.

*/

//Invoke/call the function that we declared
printName();
/*declaredFunction(); //error*/

//Function Declaration vs Function Expressions

	//Function Declaration
		//A function can be created through function declaration by using function keyword and adding function name
		//Declared function are not executed immediately. They are "saved" for later used and will be executed later when they are invoked(called).

		declaredFunction();
		function declaredFunction(){
			console.log("Hello World from the declaredFunction");
		}

		declaredFunction();


	//Function Expressions
		//A function can also be stored in a variable. This is called function expression.

		//A funtion expression is an anonymous function to the variableFunction

		//Anonymous function - a function without a name

	let variableFunction = function(){
		console.log("Hello from the variableFunction");
	}

	variableFunction();

	let funcExpression = function funcName() {
		console.log("Hello from the other side!")
	}
	funcExpression();


//Can we reassign declared funcions and function expression to a NEW anonymous functions?

	declaredFunction = function(){
		console.log("Update declaredFunction");
	}

	declaredFunction();

	funcExpression = function () {
		console.log("Updated functExpression")
	}
	funcExpression();

	//We cannot re-assigned function expression that is initialized with const
	const constantFunc = function () {
		console.log("Initialized with CONST");
	}

	constantFunc();

/*	constantFunc = function () {
		console.log("Can we reassigned?");
	}
	constantFunc();*/

//Function Scoping

/*
	Scope is the accessibility (visibility) of variables

	JS variables has 3 types of scope
		1. local/block scope
		2. global scope
		3. function scope

*/

{
	let localVar = "I am a local variable";

}

let globalVar = "I am a global variable";

console.log(globalVar);
//console.log(localVar); error


function showNames(){
	//Function scoped variables
	var functionVar = "Joe";
	const functionConst = "John";
	let funcLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(funcLet);
}

showNames();
//The variables, functionVar, functionConst, funcLet are function scoped variables. They can only be accessed inside of the function they were declared in.
	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(funcLet);

//NESTED FUNCTION
	//You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable, "name" as they are within the same code block

	function myNewFunction(){
		let name = "Maria";
		function nestedFunction(){
			let nestedName = "Jose";
			console.log(name);
		}
		//console.log(nestedName); erorr
		nestedFunction();
	}

	myNewFunction();
	// nestedFunction(); will cause error


//Function and Global Scoped Variables

	//Global Variable
	let globalName = "Erven Joshua";

	function myNewFunction2(){
		let nameInside = "Jenno";

		console.log(globalName);
	}

	myNewFunction2();
	// console.log(nameInside); Uncaught ReferenceError: nameInside is not defined


//USING ALERT()
/*
	Syntax: 
		alert(message)

*/
	//alert("Hello World");

	function showSampleAlert(){
		alert("Hello User");
	}

	//showSampleAlert();

	console.log("Hellow pows!");

	console.log("I will only log in the console when alert is dismissed!");

//USING PROMPT()

	/*
		Syntax:
			prompt("<DialogInString>");
		
	*/
	// let samplePrompt = prompt("Enter your name: ");
	// console.log("Hello, " + samplePrompt);
	// console.log(typeof samplePrompt);

// function printWelcomeMessage(){
// 	let firstName = prompt("Enter your First Name: ");
// 	let lastName = prompt("Enter your Last Name: ");

// 	// console.log("Hello, " + firstName + " " + lastName);
// 	// console.log("Welcome to my page!");
// 		alert("Hello, " + firstName + " " + lastName);
// 	alert("Welcome to my page!");

// }

// printWelcomeMessage();

//Function Naming Conventions
	//Name your functions in small caps. Follows camelCase when naming variables and functions

	function displayCarInfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1, 500, 00");
	}

	displayCarInfo();

	//Function names should be definitive of the task it will perform. It usually contains a verb.


	function getCourses(){
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}

	getCourses();

	//Avoid generic names to avoid confusion within your code

	//getName
	function get(){
		let name = "Jamie";
		console.log(name);
	}

	get();

	//Avoid pointless and inappropriate function names

	function foo(){
		console.log(25 % 5);
	}

	foo();